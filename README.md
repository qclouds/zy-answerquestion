# zy-answerQustion
建筑通--基于微信小程序的练题、答题小程序<br>包括错题、收藏、模拟考试、顺序练习。后续继续优化。


### 页面展示
 | 首页      | 模拟测试        |   题库 |
  | ----------- | ----------- |-------------|
  | ![](http://prod.dgiotcloud.com/group1/default/20220112/10/57/7/test_home)|![](http://prod.dgiotcloud.com/group1/default/20220112/10/58/7/test_ceshi)| ![](http://prod.dgiotcloud.com/group1/default/20220112/10/59/7/test_topic) |

### 小程序相关
|账号 | 密码 |
|-----|-----|
|111111111111111111|111111|

### 后台样例
| 展示       | 样例                                                                                      |
| -------------- | ----------------------------------------------------------------------------------------- |
| 试卷管理           | ![](http://prod.dgiotcloud.com/group1/default/20220112/11/39/7/test_manager)                                 |
| 试卷格式           | ![](http://prod.dgiotcloud.com/group1/default/20220112/11/40/7/test_demo)                               |

### 关于我们

| 联系方式       | 地址                                                                                      |
| -------------- | ----------------------------------------------------------------------------------------- |
| 官网           | [https://www.iotn2n.com](https://www.iotn2n.com?from=git)                                 |
| 博客           | [https://tech.iotn2n.com](https://tech.iotn2n.com?from=git)                               |
| 联系我们         | ![](https://prod.dgiotcloud.com/group1/default/20220111/15/43/7/wx)  |

## **如果你觉得对你有帮助或者觉得有价值，请不要吝惜你的star**





